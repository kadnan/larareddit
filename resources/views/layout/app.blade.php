<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{!! asset('css/app.css') !!}">
    <title>Welcome To LaraReddit</title>
</head>
<body>
    <nav class="navbar navbar-expand-sm bg-pale-gold navbar-dark">
        <a class="navbar-brand" href="#">LaraReddit</a>
        <ul class="navbar-nav">
            <li class="nav-item active">
                <a class="nav-link" href="#">Home</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">New</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Best</a>
            </li>
            <li class="nav-item">
                <a class="nav-link " href="#">Hot</a>
            </li>
        </ul>
    </nav>
    <div class="container">
        @yield('content')
    </div>
</body>
<script src="{!! asset('js/app.js') !!}"></script>
</html>
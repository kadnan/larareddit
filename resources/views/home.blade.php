@extends('layout.app')
@section('content')
    <div class="row margin-top">
        <div class="col-md-7 listing-section">
            <ul class="listing">
                <li>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-12">
                                    <i class="fas fa-caret-up"></i>
                                    <span class="score">34</span>
                                    <i class="fas fa-caret-down"></i>
                                    <h4>Had open-sourced a small script I wrote and mostly forgot..ced a small script I wrote and mostly forgot</h4> (<a class="domain" href="#">domain.com</a>)
                                </div>
                            </div>


                        </div>
                    </div>
                    <div class="row author">
                        <div class="col-md-11 offset-1">
                            <a href="#">l/resume</a> posted by <a href="#">Adnan</a> <span>10 days ago</span> | <span>12 comments</span>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="row">
                        <div class="col-md-12">
                            <i class="fas fa-caret-up"></i>
                            <span class="score">34</span>
                            <i class="fas fa-caret-down"></i>
                            <h4>Hiring manager offers to help me with my shitty resume</h4> (<a class="domain" href="#">domain.com</a>)
                        </div>
                    </div>
                    <div class="row author">
                        <div class="col-md-11 offset-1">
                            posted by <a href="#">Adnan</a> <span>10 mins ago</span> | <span>no comments</span>
                        </div>
                    </div>
                </li>

            </ul>
        </div>
        <div class="col-md-3 offset-2 bg-pale-gold right-box d-none d-sm-none d-sm-block">
                <div class="row ">
                    <div class="col-md-12">
                        <h4 class="">Home</h4>
                        <span>
                            Some redditors are skilled professionals, some redditors need skilled professionals. Scroll down for general information and our rules. Please read through these carefully, as breaking them can be a bannable offense.
                        </span>
                    </div>
                </div>
                <div class="row margin-top">
                    <div class="col-md-12">
                       <a href="{!! route('register') !!}" class="btn btn-pale-gold-dark btn-block">Join</a>
                       <button class="btn btn-pale-gold-dark btn-block">Create Post</button>
                    </div>
                </div>
        </div>
    </div>
@endsection